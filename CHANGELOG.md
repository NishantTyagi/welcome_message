## [1.2.14](https://gitlab.com/NishantTyagi/welcome_message/compare/v1.2.13...v1.2.14) (2020-11-11)


### Bug Fixes

* thanks statement removed ([6f7573c](https://gitlab.com/NishantTyagi/welcome_message/commit/6f7573c12725eb028805e54ab2b6a4691792dc50))

## [1.2.13](https://gitlab.com/NishantTyagi/welcome_message/compare/v1.2.12...v1.2.13) (2020-11-11)


### Bug Fixes

* thanks statement added ([2e11fe4](https://gitlab.com/NishantTyagi/welcome_message/commit/2e11fe4384e9c01773bcccaeabe0c42a0aa31fa5))
* thanks statement removed ([3c24149](https://gitlab.com/NishantTyagi/welcome_message/commit/3c24149b54e3d97102d4071cf1d684699757de53))

## [1.2.12](https://gitlab.com/NishantTyagi/welcome_message/compare/v1.2.11...v1.2.12) (2020-11-11)


### Bug Fixes

* Made many changes 3 ([f766c80](https://gitlab.com/NishantTyagi/welcome_message/commit/f766c8080a34aa0a54fa28febac47525295d1965))

## [1.2.11](https://gitlab.com/NishantTyagi/welcome_message/compare/v1.2.10...v1.2.11) (2020-11-11)


### Bug Fixes

* Made many changes 2 ([8e39800](https://gitlab.com/NishantTyagi/welcome_message/commit/8e39800f6dd7bec767ae04d9c5b94b62085415ec))

## [1.2.10](https://gitlab.com/NishantTyagi/welcome_message/compare/v1.2.9...v1.2.10) (2020-11-11)


### Bug Fixes

* Made many changes ([40e6171](https://gitlab.com/NishantTyagi/welcome_message/commit/40e6171d0df46a1dd9adb34752238f34aa8d54f2))

## [1.2.9](https://gitlab.com/NishantTyagi/welcome_message/compare/v1.2.8...v1.2.9) (2020-11-11)


### Bug Fixes

* Made many changes ([4391d5a](https://gitlab.com/NishantTyagi/welcome_message/commit/4391d5aa62cd660529c79285a95dcd739b80e456))

## [1.2.8](https://gitlab.com/NishantTyagi/welcome_message/compare/v1.2.7...v1.2.8) (2020-11-11)


### Bug Fixes

* minor changes made ([1acff13](https://gitlab.com/NishantTyagi/welcome_message/commit/1acff13f8616ac13ec8afb76dcc8feafc207bf12))

## [1.2.7](https://gitlab.com/NishantTyagi/welcome_message/compare/v1.2.6...v1.2.7) (2020-11-11)


### Bug Fixes

* npm publish fixed ([aee98ec](https://gitlab.com/NishantTyagi/welcome_message/commit/aee98eccbacd468c6fc78fc6141746060457dab7))

## [1.2.6](https://gitlab.com/NishantTyagi/welcome_message/compare/v1.2.5...v1.2.6) (2020-11-11)


### Bug Fixes

* publishing to npm removed 2 ([556bb81](https://gitlab.com/NishantTyagi/welcome_message/commit/556bb815273192d654ed75fbd675d312adee172e))

## [1.2.5](https://gitlab.com/NishantTyagi/welcome_message/compare/v1.2.4...v1.2.5) (2020-11-11)


### Bug Fixes

* publishing to npm removed ([f731e97](https://gitlab.com/NishantTyagi/welcome_message/commit/f731e97d8183fc372cecec5a847dd1297112a919))

## [1.2.4](https://gitlab.com/NishantTyagi/welcome_message/compare/v1.2.3...v1.2.4) (2020-11-11)


### Bug Fixes

* npm publish removed ([a43cfe1](https://gitlab.com/NishantTyagi/welcome_message/commit/a43cfe1272ff2eaf5eb5da72cac2e1f6a6d1d688))

## [1.2.3](https://gitlab.com/NishantTyagi/welcome_message/compare/v1.2.2...v1.2.3) (2020-11-11)


### Bug Fixes

* changed publish to gitlab only ([dd07f47](https://gitlab.com/NishantTyagi/welcome_message/commit/dd07f47b62edc26427bb2b08e43540a8dc9d5876))

## [1.2.2](https://gitlab.com/NishantTyagi/welcome_message/compare/v1.2.1...v1.2.2) (2020-11-11)


### Bug Fixes

* Config file changed from yaml to js ([16cc608](https://gitlab.com/NishantTyagi/welcome_message/commit/16cc6086a7dba0878985a0a2b4d007a40d6e31ec))

## [1.2.1](https://gitlab.com/NishantTyagi/welcome_message/compare/v1.2.0...v1.2.1) (2020-11-10)


### Bug Fixes

* Thanks statement added ([75c8055](https://gitlab.com/NishantTyagi/welcome_message/commit/75c8055de26e3767d63a2f24b72af4cb66842c88))

# [1.2.0](https://gitlab.com/NishantTyagi/welcome_message/compare/v1.1.2...v1.2.0) (2020-11-10)


### Features

* **logging:** Logging statement modified ([a8c132e](https://gitlab.com/NishantTyagi/welcome_message/commit/a8c132ebe0f129bff5c7b2e2aa555a4ac9ae078c))

## [1.1.2](https://gitlab.com/NishantTyagi/welcome_message/compare/v1.1.1...v1.1.2) (2020-11-10)


### Bug Fixes

* package.json file fixed ([2025bbd](https://gitlab.com/NishantTyagi/welcome_message/commit/2025bbd3a2c94d49f406edbaee17982e9fc7fcc7))
* package.json file fixed ([dfb80c5](https://gitlab.com/NishantTyagi/welcome_message/commit/dfb80c566434b1a421715003c2ef16cb0852ce88))
* version manipulated ([950bc81](https://gitlab.com/NishantTyagi/welcome_message/commit/950bc8109574381f0280ce388e71f2f145849407))

## [1.1.1](https://gitlab.com/NishantTyagi/welcome_message/compare/v1.1.0...v1.1.1) (2020-11-07)


### Bug Fixes

* logging fixed ([9eeb3c6](https://gitlab.com/NishantTyagi/welcome_message/commit/9eeb3c64e3b16017db85f860d4f72136169919bc))
* Thanks statement deleted ([cd70b8a](https://gitlab.com/NishantTyagi/welcome_message/commit/cd70b8a2645bf411a3b16af910b88cb9452030a2))

# [1.1.0](https://gitlab.com/NishantTyagi/welcome_message/compare/v1.0.0...v1.1.0) (2020-11-07)


### Features

* **Thanks Message:** Added Thanks Message ([8611aed](https://gitlab.com/NishantTyagi/welcome_message/commit/8611aed83dfcfc66d8bce443c2e15064b0ae2b93))
* **Thanks Message:** Added Thanks Message ([644500f](https://gitlab.com/NishantTyagi/welcome_message/commit/644500f3f1ca2dafedad82b67e1584f5e81d832a))

# 1.0.0 (2020-11-07)


### Features

* **Semantic Release:** Added Semantic release configuration ([1e36c7e](https://gitlab.com/NishantTyagi/welcome_message/commit/1e36c7e210336e38d2c5f3015f0e9fcb26f24787))

module.exports = {
    branches: ["master", "dev"],
    extends: ["@semantic-release/gitlab-config"],
    plugins: [
        [
            "@semantic-release/commit-analyzer",
            {
                preset: "conventionalcommits",
            },
        ],
        "@semantic-release/release-notes-generator",
        [
            "@semantic-release/changelog",
            {
                changelogFile: "docs/CHANGELOG.md",
            },
        ]
    ],
    dryRun: true,
};